package com.example.bobing;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import com.airbnb.lottie.LottieAnimationView;

import pl.droidsonroids.gif.GifImageView;


public class IntroductionAct extends AppCompatActivity {
    TextView text1, text2;
    //    Handler mhanlder = new Handler();
    Button b1;
    GifImageView wbk1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introduction);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/pm.ttf");
        text1.setTypeface(typeface);
        text2.setTypeface(typeface);
        wbk1 = findViewById(R.id.wbk1);
        b1 = (Button) findViewById(R.id.Button1);

        b1.setTypeface(typeface);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(IntroductionAct.this, MainActivity.class);
                startActivity(intent);
            }
        });

    }
}