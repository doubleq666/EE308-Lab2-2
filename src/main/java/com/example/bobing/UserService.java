package com.example.bobing;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class UserService {
    private MyDBHelper dbHelper;
    public UserService(Context context){
        dbHelper=new MyDBHelper(context);
    }

    public boolean login(String zhanghao,String mima){
        SQLiteDatabase sdb=dbHelper.getReadableDatabase();
        String sql="select * from yonghu where zhanghao=? and mima=?";
        Cursor cursor=sdb.rawQuery(sql, new String[]{zhanghao,mima});
        if(cursor.moveToFirst()==true){
            cursor.close();
            return true;
        }
        return false;
    }
}
