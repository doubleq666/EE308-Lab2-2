package com.example.bobing;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import android.os.Handler;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.util.Random;

public class Mul extends AppCompatActivity {
    WebView webView;
    int cnt = 0;
    int anInt = 3;
    Button fh3, qr;
    TextView nc, fs, lk;
    EditText text;
    ImageView logo;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mul_layout);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/pm.ttf");
        webView = (WebView) findViewById(R.id.webview1);
        fh3 = findViewById(R.id.fh3);
        qr = findViewById(R.id.qr);
//        fg = findViewById(R.id.fg);
        nc = findViewById(R.id.nc1);
        text = findViewById(R.id.nc2);
        fs = findViewById(R.id.fs);
        lk = findViewById(R.id.lk);
        lk.setTypeface(typeface);
        logo = findViewById(R.id.logo1);
        fs.setTypeface(typeface);
        Button pm = findViewById(R.id.pm);
        Button btn = findViewById(R.id.button666);
        btn.setTypeface(typeface);
        pm.setTypeface(typeface);
        qr.setTypeface(typeface);
        nc.setTypeface(typeface);
        //fg.setVisibility(fg.INVISIBLE);
        lk.setVisibility(lk.INVISIBLE);
        btn.setVisibility(btn.INVISIBLE);
        pm.setVisibility(pm.INVISIBLE);
        webView.setVisibility(webView.INVISIBLE);
        qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!String.valueOf(text.getText()).equals("") && String.valueOf(text.getText()).length() < 5) {
                    qr.setVisibility(fh3.INVISIBLE);
                    nc.setVisibility(fh3.INVISIBLE);
                    text.setVisibility(fh3.INVISIBLE);
                    btn.setVisibility(btn.VISIBLE);
                    pm.setVisibility(pm.VISIBLE);
                    webView.setVisibility(webView.VISIBLE);
                } else if (String.valueOf(text.getText()).equals("")) {
                    Toast.makeText(Mul.this, "昵称不能为空", Toast.LENGTH_LONG).show();
                } else if (String.valueOf(text.getText()).length() > 4) {
                    Toast.makeText(Mul.this, "昵称不能超过4个字", Toast.LENGTH_LONG).show();
                }
            }
        });

        pm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(Mul.this, Pm.class);
                startActivity(intent);
            }
        });
        fh3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        webView.loadUrl("file:///android_asset/bobing/index.html");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient());
        webView.setBackgroundColor(0);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int a = Math.abs(new Random().nextInt()) % 6 + 1;
                int b = Math.abs(new Random().nextInt()) % 6 + 1;
                int c = Math.abs(new Random().nextInt()) % 6 + 1;
                int d = Math.abs(new Random().nextInt()) % 6 + 1;
                int e = Math.abs(new Random().nextInt()) % 6 + 1;
                int f = Math.abs(new Random().nextInt()) % 6 + 1;
                webView.loadUrl("javascript:begin(" + a + ", " + b + ", " + c + ", " + d + ", " + e + ", " + f + ")");
                anInt--;
                s = getString(a, b, c, d, e, f);
                if (anInt > 0) {
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            fg.setVisibility(fg.VISIBLE);
//                        }
//                    },2000);
//                    fg.setVisibility(fg.INVISIBLE);
                    cnt += getScore(a, b, c, d, e, f);
                    fs.setText("你还有" + anInt + "次机会" + "\r\n" + getString(a, b, c, d, e, f));
                    logo.setVisibility(View.GONE);
                } else if (anInt <= 0) {
                    cnt += getScore(a, b, c, d, e, f);
                    OkHttpClient client = new OkHttpClient.Builder().build();
                    fs.setText("你还有0次机会" + "\r\n" + s);
                    Request request = new Request.Builder().url("https://www.doubleq666.cn/bobing_xing/submit-rank?nickname=" + String.valueOf(text.getText()) + "&score=" + cnt).build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(@NonNull Call call, @NonNull IOException e) {
                        }

                        @Override
                        public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                            //Log.d("OkHttp", response.body().string());
                        }
                    });
                    btn.setVisibility(btn.INVISIBLE);
                    lk.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public String getString(int a, int b, int c, int d, int e, int f) {
        int[] cnt = new int[10];
        cnt[1] = cnt[2] = cnt[3] = cnt[4] = cnt[5] = cnt[6] = 0;
        cnt[a]++;
        cnt[b]++;
        cnt[c]++;
        cnt[d]++;
        cnt[e]++;
        cnt[f]++;
        if (cnt[1] == 2 && cnt[4] == 4) {
            return "状元插金花 +800";
        } else if (cnt[4] == 6) {
            return "六杯红 +500";
        } else if (cnt[1] == 6) {
            return "遍地锦 +500";
        } else if (cnt[4] == 5 && cnt[1] == 1) {
            return "五红 +400";
        } else if (cnt[1] == 5 || cnt[2] == 5 || cnt[3] == 5 || cnt[4] == 5 || cnt[5] == 5 || cnt[6] == 5) {
            return "五子登科 +300";
        } else if (cnt[6] == 5) {
            return "五红 +200";
        } else if (cnt[4] == 4) {
            return "四点红 +100";
        } else if (cnt[1] == 1 && cnt[2] == 1 && cnt[3] == 1 && cnt[4] == 1 && cnt[5] == 1 && cnt[6] == 1) {
            return "榜眼 +80";
        } else if (cnt[4] == 3) {
            return "三红 +60";
        } else if (cnt[1] == 4 || cnt[2] == 4 || cnt[3] == 4 || cnt[5] == 4 || cnt[6] == 4) {
            return "四进 +40";
        } else if (cnt[4] == 2) {
            return "二秀 +20";
        } else if (cnt[4] == 1) {
            return "一秀 +10";
        }
        return "虾米拢某 +0";
    }

    public int getScore(int a, int b, int c, int d, int e, int f) {
        int[] cnt = new int[10];
        cnt[1] = cnt[2] = cnt[3] = cnt[4] = cnt[5] = cnt[6] = 0;
        cnt[a]++;
        cnt[b]++;
        cnt[c]++;
        cnt[d]++;
        cnt[e]++;
        cnt[f]++;
        if (cnt[1] == 2 && cnt[4] == 4) {
            return 800;
        } else if (cnt[4] == 6) {
            return 500;
        } else if (cnt[1] == 6) {
            return 500;
        } else if (cnt[4] == 5 && cnt[1] == 1) {
            return 400;
        } else if (cnt[1] == 5 || cnt[2] == 5 || cnt[3] == 5 ||cnt[4]==5|| cnt[5] == 5 || cnt[6] == 5) {
            return 300;
        } else if (cnt[6] == 5) {
            return 200;
        } else if (cnt[4] == 4) {
            return 100;
        } else if (cnt[1] == 1 && cnt[2] == 1 && cnt[3] == 1 && cnt[4] == 1 && cnt[5] == 1 && cnt[6] == 1) {
            return 80;
        } else if (cnt[4] == 3) {
            return 60;
        } else if (cnt[6] == 4) {
            return 40;
        } else if (cnt[4] == 2) {
            return 20;
        } else if (cnt[4] == 1) {
            return 10;
        }
        return 0;
    }
}
