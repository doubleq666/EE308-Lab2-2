package com.example.bobing;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bobing.MainActivity;
import com.example.bobing.MyDBHelper;
import com.mob.MobSDK;

import org.w3c.dom.Text;

import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;

public class register extends AppCompatActivity {
    TextView text1,text2,text3;
    EditText phone;
    EditText cord;
    Button getCord;
    Button saveCord;
    Button fh;
    MyDBHelper mhelper;
    SQLiteDatabase db;
    final register.MyCountDownTimer myCountDownTimer = new register.MyCountDownTimer(60000,1000);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();/*绑定控件*/
        fh.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
        MobSDK.submitPolicyGrantResult(true, null);/*调用smssdk接口*/
        btnOnClick();/*按钮单击事件*/
    }

    public void regist(){   //接口回调函数
        EventHandler eh = new EventHandler() {
            @Override
            public void afterEvent(int event, int result, Object data) {
                if (result == SMSSDK.RESULT_COMPLETE) {   //回调完成
                    if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ContentValues values=new ContentValues();
                                values.put("zhanghao",phone.getText().toString());
                                values.put("mima",cord.getText().toString());
                                db.insert("yonghu",null,values);
                                Toast.makeText(getApplication(), "您已验证成功,注册成功，请登录~~", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(register.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                    } else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {    //如果获取到验证码
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplication(), "已发送验证码，请注意查收", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {}    //返回支持发送验证码的国家列表
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplication(), "操作失败，重新获取验证码", Toast.LENGTH_SHORT).show();
                        }
                    });
                    ((Throwable) data).printStackTrace();
                }
            }
        };
        SMSSDK.registerEventHandler(eh); //注册短信回调
        saveCord.setVisibility(View.VISIBLE);
        myCountDownTimer.start();
    }

    private void btnOnClick() {
        getCord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                regist();//调用注册短信发送的回调接口
                String a = phone.getText().toString().trim();//获取输入的手机号
                if (TextUtils.isEmpty(a))//判断字符串是null或者是“”
                {
                    Toast.makeText(getApplicationContext(), "输入的手机号不能为空",Toast.LENGTH_LONG).show();
                } else {
                    SMSSDK.getVerificationCode("86", a);
                }
            }
        });
        saveCord.setOnClickListener(new View.OnClickListener() {//验证注册
            @Override
            public void onClick(View view) {
                String phoneNumber = phone.getText().toString().trim();//获取输入的手机号
                String code = cord.getText().toString().trim();//获取输入的验证码
                if (TextUtils.isEmpty(phoneNumber) && TextUtils.isEmpty(code)) {
                    Toast.makeText(getApplicationContext(), "请检验您输入的信息", Toast.LENGTH_LONG).show();
                }
                else
                {
                    SMSSDK.submitVerificationCode("86",phoneNumber,code);//对比
                }
            }
        });
    }
    private void init() {
        phone = (EditText) findViewById(R.id.phone);
        cord = (EditText) findViewById(R.id.cord);
        getCord = (Button) findViewById(R.id.getcord);
        saveCord = (Button) findViewById(R.id.savecord);
        fh = (Button)findViewById(R.id.fh);
        text1 = findViewById(R.id.textView1);
        text2 = findViewById(R.id.textView2);
        text3 = findViewById(R.id.textView3);
        mhelper=new MyDBHelper(register.this);
        db=mhelper.getWritableDatabase();

        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/pm.ttf");
        text1.setTypeface(typeface);
        text2.setTypeface(typeface);
        text3.setTypeface(typeface);
        getCord.setTypeface(typeface);
        saveCord.setTypeface(typeface);
    }


    private class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        //计时过程
        @Override
        public void onTick(long l) {
            //防止计时过程中重复点击
            getCord.setClickable(false);
            getCord.setBackgroundColor(Color.parseColor("#00000000"));
            getCord.setText(l / 1000 + "秒");
        }

        //计时完毕的方法
        @Override
        public void onFinish() {
            //重新给Button设置文字
            getCord.setText("重新获取");
            //设置可点击
            getCord.setClickable(true);
        }
    }
}


