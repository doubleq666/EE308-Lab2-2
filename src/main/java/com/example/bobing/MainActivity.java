package com.example.bobing;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.bobing.MyDBHelper;
import com.example.bobing.UserService;
import com.example.bobing.register;

public class MainActivity extends AppCompatActivity {

    EditText et_zhanghao,et_mima;
    Button bt_denglu,bt_zhuce;
    TextView tv_zhaohao,tv_password,text;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    CheckBox remember;
    MyDBHelper mhelper;
    SQLiteDatabase db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();//绑定控件
        /*以下用来实现记住密码的功能*/
        preferences= PreferenceManager.getDefaultSharedPreferences(this);
        boolean isRemember=preferences.getBoolean("remember_password",false);
        if (isRemember){
            String account=preferences.getString("account","");
            String password=preferences.getString("password","");
            et_zhanghao.setText(account);
            et_mima.setText(password);
            remember.setChecked(true);
        }
        bt_onclick();//按钮单击事件
    }
    private void bt_onclick() {
        bt_zhuce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, register.class);
                startActivity(intent);//简单的跳转一下
            }
        });
        bt_denglu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*检测登录*/
                String zhanghao = et_zhanghao.getText().toString().trim();
                String mima = et_mima.getText().toString().trim();
                if (!TextUtils.isEmpty(zhanghao) && !TextUtils.isEmpty(mima)) {
                    UserService uService=new UserService(MainActivity.this);
                    boolean flag=uService.login(zhanghao, mima);//账号和密码正确的话，flag为true
                    if(flag){
                        /*记住密码*/
                        Toast.makeText(MainActivity.this, "登录成功", Toast.LENGTH_LONG).show();
                        editor=preferences.edit();
                        if (remember.isChecked()){
                            editor.putBoolean("remember_password",true);
                            editor.putString("account", zhanghao);
                            editor.putString("password",mima);
                        }else{
                            editor.clear();
                        }
                        editor.apply();
                        /*跳转*/
                        Intent intent = new Intent(MainActivity.this,HallAct.class);
                        intent.putExtra("zh",zhanghao);//传值，以用来在登录完后修改密码
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(MainActivity.this, "密码或者账号错误，请重新登录", Toast.LENGTH_LONG).show();
                    }
                } else{ Toast.makeText(MainActivity.this, "账号或者密码不能为空" , Toast.LENGTH_SHORT).show();}
            }
        });
    }
    private void init() {
        et_zhanghao = findViewById(R.id.et_zhanghao);
        et_mima = findViewById(R.id.et_mima);
        bt_denglu = findViewById(R.id.bt_denglu);
        bt_zhuce = findViewById(R.id.bt_zhuce);
        tv_zhaohao = findViewById(R.id.tv_number);
        tv_password = findViewById(R.id.tv_password);
        text = findViewById(R.id.tx);
        remember=(CheckBox) findViewById(R.id.checkBox);
        mhelper=new MyDBHelper(MainActivity.this);
        db=mhelper.getWritableDatabase();
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/pm.ttf");
        tv_zhaohao.setTypeface(typeface);
        tv_password.setTypeface(typeface);
        bt_denglu.setTypeface(typeface);
        bt_zhuce.setTypeface(typeface);
        text.setTypeface(typeface);
    }
}
