package com.example.bobing;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import pl.droidsonroids.gif.GifImageView;

public class game extends AppCompatActivity {
    WebView webView;
    Button fh1;
    Handler mhanlder = new Handler();
    GifImageView drbj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_layout);
        drbj = findViewById(R.id.drbj);
        fh1 = (Button) findViewById(R.id.fh1);
        fh1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
        webView = (WebView) findViewById(R.id.webview1);
        webView.setBackgroundColor(0);
        webView.loadUrl("https://preview.cocos.com/dms/286/1662369521/index.html");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setVisibility(webView.INVISIBLE);
        mhanlder.postDelayed(new Runnable() {
            @Override
            public void run() {
                drbj.setVisibility(drbj.GONE);
                webView.setVisibility(webView.VISIBLE);
            }
        }, 12000);

    }
}
