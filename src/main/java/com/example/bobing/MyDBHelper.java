package com.example.bobing;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDBHelper extends SQLiteOpenHelper {
    private static  final String DBNAME="MyData";
    private static final int VERSION=1;
    private SQLiteDatabase db;
    public MyDBHelper(Context context) {
        super(context, DBNAME, null, VERSION);
    }
    //1 创建数据库
    @Override
    public void onCreate(SQLiteDatabase db) {
        //创建用户表，储存账号和密码
        db.execSQL("create table yonghu(id integer primary key autoincrement,zhanghao varchar(10),mima varchar(15))");
    }
    //2 升级数据库
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}

