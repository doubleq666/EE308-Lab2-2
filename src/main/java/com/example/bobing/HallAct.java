package com.example.bobing;



import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;


public class HallAct extends AppCompatActivity {
    Button danr,duor,sz;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hall_fragment);
        danr = (Button) findViewById(R.id.danren);
        duor = (Button) findViewById(R.id.duoren);
        sz = (Button) findViewById(R.id.sz);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/pm.ttf");
        danr.setTypeface(typeface);
        duor.setTypeface(typeface);
        sz.setTypeface(typeface);

        danr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(HallAct.this, game.class);
                startActivity(intent);
            }
        });
        duor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(HallAct.this, Mul.class);
                startActivity(intent);
            }
        });
        sz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(HallAct.this, SetAct.class);
                startActivity(intent);
            }
        });

    }

}
